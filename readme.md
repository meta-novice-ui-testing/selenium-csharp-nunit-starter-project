[![MetaMarcus Logo](https://streamerlinks.com/uploads/avatars/thumbs/13c81cfedfd10b4c91f3969ff5739250.png "MetaMarcus Logo")](http://streamerlinks.com/metamarcus "MetaMarcus Logo")
# Selenium CSharp NUnit Starter Project

This project is to assist someone trying to get started writing Automated UI Tests using C#, NUnit, and Selenium WebDriver. Feel free to fork or clone this and customize it for your own testing needs.

### Support
For assistance, join my Software Test Automation community on Discord:
https://discord.gg/CWCmeW2Kqu

### SubscribeStar
If you found this or any of my other projects useful or would like premium Test Automation support, consider supporting me on my SubscribeStar page:
https://subscribestar.com/metamarcus

### Other Links
I have other resources and videos around the subject of Software Test Automation: https://streamerlinks.com/metamarcus

## Dependencies
- NUnit
	 - NUnit is a unit-testing framework.
	 - https://nunit.org/
- Selenium WebDriver
	 - Selenium WebDriver is a library that allows you to manipluate browsers the way a user would.
	 - https://www.selenium.dev/
- WebDriverManager
	 - WebDriverManager is a library that allows you to automatically manage your local Selenium WebDriver binaries.
	 - https://github.com/rosolko/WebDriverManager.Net

## Running The Demo Tests
The tests included in this project as examples run against https://www.saucedemo.com/. It is a site provided by SauceLabs for practicing UI Test Automation. I am not liable for any changes they make to the site that would invalidate a clean run of the example tests.

By default the tests are configured to run against chrome so make sure you have chrome installed.

### Using Visual Studio
The easiest way to run the tests is to open the csproj in Visual Studio.
1. Open the Test Explorer in Visual Studio
2. Hit the green play button (Run All Tests In View)

### NUnit Test Runners
NUnit provides a command line and gui test runner.
The most up to date documentation on this is here:
https://docs.nunit.org/articles/nunit/running-tests/Index.html

------------

Copyright &copy; MetaMarcus 2020