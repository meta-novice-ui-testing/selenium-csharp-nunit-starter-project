﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumCSharpNUnitStarterProject.Utilities
{
    public enum BrowserType
    {
        InternetExplorer,
        Firefox,
        Chrome,
        Edge
    }
}
