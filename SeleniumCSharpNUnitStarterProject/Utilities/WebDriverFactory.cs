﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

// This factory implements WebDriverManager to manage Selenium Webdriver Binaries
// This helps you to avoid having to store and maintain them yourself
// For more info: https://github.com/rosolko/WebDriverManager.Net

namespace SeleniumCSharpNUnitStarterProject.Utilities
{
    public static class WebDriverFactory
    {
        public static IWebDriver CreateWebDriver(BrowserType browser) {
            IWebDriver driver = null;

            switch (browser) {
                case BrowserType.InternetExplorer:
                    new DriverManager().SetUpDriver(new InternetExplorerConfig());
                    driver = new InternetExplorerDriver();
                    break;
                case BrowserType.Firefox:
                    new DriverManager().SetUpDriver(new FirefoxConfig());
                    driver = new FirefoxDriver();
                    break;
                case BrowserType.Edge:
                    new DriverManager().SetUpDriver(new EdgeConfig());
                    driver = new EdgeDriver();
                    break;
                case BrowserType.Chrome:
                    new DriverManager().SetUpDriver(new ChromeConfig());
                    driver = new ChromeDriver();
                    break;
            }

            return driver;
        }
    }
}
