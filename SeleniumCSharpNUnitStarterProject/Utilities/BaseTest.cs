﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumCSharpNUnitStarterProject.Utilities
{
    public class BaseTest
    {
        public IWebDriver driver;

        [SetUp]
        public void Setup() {
            this.driver = WebDriverFactory.CreateWebDriver(BrowserType.Chrome);
            this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            this.driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown() {
            this.driver.Quit();
        }
    }
}
