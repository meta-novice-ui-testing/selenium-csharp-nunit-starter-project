using NUnit.Framework;
using SeleniumCSharpNUnitStarterProject.PageObjects.SauceDemo;
using SeleniumCSharpNUnitStarterProject.Utilities;

namespace SeleniumCSharpNUnitStarterProject
{
    public class LoginTests: BaseTest
    {
        private SauceLoginPage SauceLoginPage;

        [SetUp]
        public void SetUp() {
            this.driver.Url = "https://www.saucedemo.com/";
            SauceLoginPage = new SauceLoginPage(this.driver);
        }

        [Test, Order(1)]
        public void LoginPageReadyTest() {
            Assert.True(
                SauceLoginPage
                .LoginElementsDisplayed()
                );
        }

        [Test]
        public void ValidCredentialsLoginTest()
        {
            Assert.True(
                SauceLoginPage
                .ValidLogin("standard_user", "secret_sauce")
                .GetCurrentUrl()
                .Contains("inventory.html")
                );
        }

        [TestCase("", "", "Username is required")]
        [TestCase("standard_user", "", "Password is required")]
        [TestCase("standard_user", "asdf1234", "Username and password do not match any user in this service")]
        public void InvalidCrecentialsLoginTests(string username, string password, string expError) {
            Assert.True(
                SauceLoginPage
                .InvalidLogin(username, password)
                .ErrorFieldIsDisplayed()
                );
            Assert.True(
                SauceLoginPage
                .GetErrorFieldText()
                .Contains(expError)
                );
        }
    }
}