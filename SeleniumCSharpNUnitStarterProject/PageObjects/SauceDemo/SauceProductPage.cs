﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumCSharpNUnitStarterProject.PageObjects.SauceDemo
{
    public class SauceProductPage: BasePage
    {
        private IWebDriver driver;

        public SauceProductPage(IWebDriver driver) : base(driver) {
            this.driver = driver;
        }
    }
}
