﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumCSharpNUnitStarterProject.PageObjects.SauceDemo
{
    public class SauceLoginPage: BasePage
    {
        private IWebDriver driver;

        private readonly By UsernameFieldLocator = By.Id("user-name");
        private readonly By PasswordFieldLocator = By.Id("password");
        private readonly By LoginButtonLocator = By.Id("login-button");
        private readonly By ErrorFieldLocator = By.XPath("//h3[@data-test='error']");

        public SauceLoginPage(IWebDriver driver) : base(driver){
            this.driver = driver;
        }

        private IWebElement UsernameField {
            get { return this.driver.FindElement(UsernameFieldLocator); }
        }

        private IWebElement PasswordField {
            get { return this.driver.FindElement(PasswordFieldLocator); }
        }

        private IWebElement LoginButton {
            get { return this.driver.FindElement(LoginButtonLocator); }
        }

        private IWebElement ErrorField {
            get { return this.driver.FindElement(ErrorFieldLocator); }
        }

        public bool LoginElementsDisplayed() {
            bool loginElementsDisplayed = false;
            if (UsernameField.Displayed & PasswordField.Displayed & LoginButton.Displayed) {
                loginElementsDisplayed = true;
            }
            return loginElementsDisplayed;
        }

        public bool ErrorFieldIsDisplayed() {
            return this.ErrorField.Displayed;
        }

        public string GetErrorFieldText() {
            return this.ErrorField.Text;
        }

        public SauceLoginPage InvalidLogin(string username, string password) {
            AttemptLogin(username, password);
            return this;
        }

        public SauceProductPage ValidLogin(string username, string password) {
            AttemptLogin(username, password);
            return new SauceProductPage(this.driver);
        }

        private void AttemptLogin(string username, string password) {
            this.UsernameField.SendKeys(username);
            this.PasswordField.SendKeys(password);
            this.LoginButton.Click();
        }
    }
}
