﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumCSharpNUnitStarterProject.PageObjects
{
    public class BasePage
    {
        private IWebDriver driver;

        public BasePage(IWebDriver driver) {
            this.driver = driver;
        }

        public string GetCurrentUrl() {
            return this.driver.Url;
        }    
    }
}
